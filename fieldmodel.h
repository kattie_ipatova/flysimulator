#ifndef FIELDMODEL_H
#define FIELDMODEL_H

#include <QAbstractListModel>
#include <QSharedPointer>
#include <QPoint>
#include <QStack>
#include  <QThread>

#include "flyobject.h"
#include "sharedobjects.h"
#include "flycontroller.h"
#include "structures.h"

/**
 * @brief МОдель представления поля
 */

class FieldModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(int columns READ columns NOTIFY columnsChanged)
    Q_PROPERTY(bool canAddFly READ canAddFly NOTIFY canAddFlyChanged)
public:
    FieldModel(QObject* parent = nullptr);
    ~FieldModel();

    int rowCount(const QModelIndex &_parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &_index, int _role) const override;

    enum ItemRoles
    {
        DeadCountRole = Qt::UserRole + 1,
        AliveCountRole
    };

    QHash<int, QByteArray> roleNames() const override;

    int columns() const;
    bool canAddFly() const;

public slots:
    void resize(qint32 _matrixSize, qint32 _flyCapacity, qint32 _stupidityMsec);
    void addFly();

private slots:
    //Обновление информации о ячейке, отправка соответствующих сигналов
    void handleAddFly(int _index);
    void handleFlyMoved(int _from, int _to);
    void handleFlyDied(int _index);

private:
    bool isIndexCorrect(const QModelIndex &_index) const;

private:

    QSharedPointer<QMap<qint32, FliesInCell> > m_cellMap;

    FlyController* m_flyController;
    QThread* m_flyControllerThread;
signals:
    void columnsChanged();
    void canAddFlyChanged();
};

#endif // FIELDMODEL_H
