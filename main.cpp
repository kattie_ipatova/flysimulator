#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "fieldmodel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType<FieldModel>("FieldModelPackage", 1, 0, "FieldModel");
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
