#ifndef RANDOMGENERATOR_H
#define RANDOMGENERATOR_H

#include <random>
#include <QTime>

#include "sharedobjects.h"

/// Генераторы случайности для поля

class RandomGenerator
{
public:
    static RandomGenerator &instance();
    unsigned int getValue(int min, int max);

protected:
    RandomGenerator()
        : m_engine(QTime::currentTime().msec())
    {}

private:
    std::default_random_engine m_engine;
};


class DirectionGenerator : private RandomGenerator
{
public:    
    static Direction::Direction getDirection();

private:
    DirectionGenerator();
};

#endif // RANDOMGENERATOR_H
