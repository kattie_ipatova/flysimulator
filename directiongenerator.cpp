#include "directiongenerator.h"

#include <limits>


Direction::Direction DirectionGenerator::getDirection()
{
    return Direction::Direction(RandomGenerator::instance().getValue(0, 4));
}

DirectionGenerator::DirectionGenerator()
{}

RandomGenerator &RandomGenerator::instance()
{
    static RandomGenerator _generator;
    return _generator;
}


unsigned int RandomGenerator::getValue(int min, int max)
{
    std::uniform_int_distribution<int> _distribution(min, max);
    return _distribution(m_engine);
}

