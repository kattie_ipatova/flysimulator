#include "flycontroller.h"
#include <QDebug>

#include <QtConcurrent/QtConcurrent>

FlyController::FlyController(WeakFlyField _flyField, QObject *parent)
    : QObject(parent), m_flyField(_flyField), m_flyCount(0), m_fliesFinished(0)
{}

FlyController::~FlyController()
{
    while (m_fliesFinished.load() != m_flyCount.load())
    {
        QThread::currentThread()->msleep(3);
        qDebug() << m_fliesFinished.load() << m_flyCount.load();
    }
}

void FlyController::resize()
{
    //TODO - check locks
    auto size = GeneralParameters::instance().matrixSize();
    size *= size;
    m_cellLockers.clear();
    for (int i = 0; i < size; ++i)
        m_cellLockers.push_back(Mutex_ptr(new QMutex()));

    QThreadPool::globalInstance()->setMaxThreadCount(size + 5);
}

void FlyController::addFly()
{
    const int  columnSize = GeneralParameters::instance().matrixSize();
    auto _cell = findCell();

    if (_cell >= 0)
    {
        FlyObject_ptr _newFly(new FlyObject);
        connect(_newFly.data(), &FlyObject::wannaMove, this, [=](Direction::Direction _movingDirection)->void
        {

            //Race - to fix!
            auto _to = m_flies.value(_newFly);
            switch (_movingDirection)
            {
            case Direction::Down:
            {
                _to += columnSize;
            } break;
            case Direction::Up:
            {
                _to -= columnSize;
            } break;
            case Direction::Left:
            {
                --_to;
            } break;
            case Direction::Right:
            {
                ++_to;
            } break;
            }
            if (_to >= 0 && _to < columnSize * columnSize)
                handleMoving(_newFly, _to, m_flies.value(_newFly));
            else
                qDebug() << "Requested to move, but can\'t move to" << _to;
        }, Qt::QueuedConnection);

        connect(this, &FlyController::s_needStop, _newFly.data(), &FlyObject::stop);

        connect(_newFly.data(), &FlyObject::s_finished, [this, _newFly]()->void
        {
            ++m_fliesFinished;
                    qDebug() << "Handle fly died in controller";
                    emit flyDied(m_flies.value(_newFly));
                    m_flies.remove(_newFly);
        });

        QtConcurrent::run(_newFly.data(), &FlyObject::makeLifePath);

        m_flies.insert(_newFly, _cell);
    }
}

void FlyController::handleMoving(FlyObject_ptr _fly, int _to, int _from)
{
    bool done = false;
    qDebug() << "Changing fly position from" << _from << "to" << _to;
    auto _strongField = m_flyField.toStrongRef();
    while (!done)
    {
        qDebug() << "Locking cells..." << _from << _to;
        if (m_cellLockers.at(_to)->tryLock(RandomGenerator::instance().getValue(0, 11)))
        {
            if (_strongField->value(_to).capacity() < GeneralParameters::instance().flyCapacity())
            {
                if (m_cellLockers.at(_from)->tryLock(RandomGenerator::instance().getValue(0, 5)))
                {
                    m_flies[_fly] = _to;
                    m_cellLockers.at(_from)->unlock();
                    done = true;
                    emit flyMoved(_from, _to);
                }
            }
            else
            {
                qDebug() << "Can\'t move from" << _from << "to" << _to << " cause of capacity limit";
                done = true;
            }

            m_cellLockers.at(_to)->unlock();
        }
    }
}

int FlyController::totalFlyCount() const
{
    return m_flyCount.load();
}

bool FlyController::canAddFly() const
{
    return (m_flyCount.load() < GeneralParameters::instance().maxFlies());
}

void FlyController::stop()
{
    emit s_needStop();
}

int FlyController::findCell()
{
    int _result = -1;
    const int  columnSize = GeneralParameters::instance().matrixSize();
    auto _strongField = m_flyField.toStrongRef();
    if (_strongField && canAddFly())
    {
        while (_result < 0)
        {
            _result = RandomGenerator::instance().getValue(0, columnSize*columnSize-1);

            qDebug() << "Add to cell" << _result;
            QMutexLocker _locker(m_cellLockers[_result].data());

            if (_strongField->value(_result).capacity() < GeneralParameters::instance().flyCapacity())
            {
                qDebug() << "Capacity in cell checked, adding fly";
                ++m_flyCount;
                emit flyAdded(_result);
            }
            else
            {
                qWarning() << "Capacity is too big!";
                _result = -1;
            }

        }
    }

    return _result;
}
