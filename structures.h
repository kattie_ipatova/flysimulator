#ifndef STRUCTURES_H
#define STRUCTURES_H

#include "flyobject.h"
#include <QSharedPointer>
#include <QMutex>


typedef QSharedPointer<FlyObject> FlyObject_ptr;
typedef QSharedPointer<QMutex> Mutex_ptr;

struct FliesInCell
{
    std::atomic<int> aliveCount;
    std::atomic<int> deadCount;
    int capacity() const
    {
        return aliveCount + deadCount;
    }
    FliesInCell(int _alive = 0, int _dead = 0)
        : aliveCount(_alive), deadCount(_dead)
    {}

    FliesInCell(const FliesInCell& _other)
        : aliveCount(_other.aliveCount.load()), deadCount(_other.deadCount.load())
    {}

    FliesInCell& operator =(const FliesInCell& _other)
    {
        aliveCount.store(_other.aliveCount.load());
        deadCount.store(_other.deadCount.load());
        return (*this);
    }
};

struct MoveRequest
{
    FlyObject_ptr fly;
    int from;
    int to;


    MoveRequest(FlyObject_ptr _fly, int _from, int _to)
        : fly(_fly), from(_from), to(_to)
    {}
};

struct FlyPosition
{
    FlyObject_ptr _fly;
    int index;
};


class CellUnlocker
{
public:
    CellUnlocker(QVector<Mutex_ptr> _mutexes)
        : m_mutexes(_mutexes)
    {}
    ~CellUnlocker()
    {
        for (auto _mutex : m_mutexes)
            _mutex->unlock();
    }

private:
    QVector<Mutex_ptr> m_mutexes;
};

#endif // STRUCTURES_H
