#ifndef SHAREDOBJECTS_H
#define SHAREDOBJECTS_H

#include <qglobal.h>
#include <QObject>

class GeneralParameters
{
    friend class FieldModel;

public:
    qint32 stupidityMsec() const
    {
        return m_stupidityMsec;
    }
    qint32 matrixSize()
    {
        return m_matrixSize;
    }
    qint32 flyCapacity() const
    {
        return m_flyCapacity;
    }
    qint32 maxFlies() const
    {
        return m_matrixSize*m_matrixSize*m_flyCapacity;
    }

    static GeneralParameters& instance()
    {
        static GeneralParameters _instance;
        return _instance;
    }

private:
    GeneralParameters()
        : m_stupidityMsec(0), m_matrixSize(0), m_flyCapacity(0)
    {}

private:
    qint32 m_stupidityMsec;
    qint32 m_matrixSize;
    qint32 m_flyCapacity;
};


namespace Direction
{
enum Direction
{
    Up = 0,
    Down,
    Left,
    Right
};
}

#endif // SHAREDOBJECTS_H
