import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4 as OldControls
import QtQuick.Layouts 1.0
import FieldModelPackage 1.0
import QtQuick.Controls.Material 2.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Fly simulator")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: 0
        interactive: false

        Page {

            ColumnLayout {
                anchors.centerIn: parent

                GridLayout {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    columns: 2

                    Text {
                        Layout.alignment: Qt.AlignVCenter
                        text: "Мухоёмкость"
                        font.pixelSize: 15
                    }

                    SpinBox {
                        id: capacity
                        from: 1
                        value: 7
                        editable: true

                    }

                    Text {
                        Layout.alignment: Qt.AlignVCenter
                        text: "Размер поля"
                        font.pixelSize: 15

                    }

                    SpinBox {
                        id: matrixSize
                        from: 2
                        value: 2
                        editable: true
                    }

                    Text {
                        Layout.alignment: Qt.AlignVCenter
                        text: "Степень тупости"
                        font.pixelSize: 15
                    }

                    SpinBox {
                        id: dummyDegree
                        from: 100
                        to: 10000
                        value: 1000
                        stepSize: 100
                        editable: true
                    }

                }

                Button {
                    id: createFieldButton
                    Layout.alignment: Qt.AlignHCenter
                    Layout.preferredWidth: parent.width / 2
                    //Layout.fillWidth: true
                    text: qsTr("Создать")
                    font.pixelSize: 16

                    onClicked: {
                        console.log("Matrix size:" , matrixSize.value,
                                    "\nCapacity:", capacity.value,
                                    "\nDummyDegree:", dummyDegree.value)
                        fieldModel.resize(matrixSize.value, capacity.value, dummyDegree.value)
                        swipeView.setCurrentIndex(1)
                    }
                }
            }
        }

        Page {

            ColumnLayout {
                anchors.fill: parent
                anchors.horizontalCenter: parent.horizontalCenter

                Grid {
                    id: fieldGrid
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    columns: fieldModel.columns
                    Repeater {
                        id: rowRepeater
                        model: fieldModel

                        Rectangle {
                            width: (fieldGrid.width / fieldGrid.columns) > 50 ? (fieldGrid.width / fieldGrid.columns) : 50
                            height: (fieldGrid.height / fieldGrid.columns) > 50 ? (fieldGrid.height / fieldGrid.columns) : 50
                            color: "#ffeed4"
                            border.color: "#340000"
                            Text {
                                anchors.right: parent.right
                                anchors.rightMargin: 3
                                anchors.bottom: parent.bottom
                                anchors.bottomMargin: 5
                                color: "red"
                                text: dead
                                visible: text !== "" && dead !== 0
                            }

                            Text {
                                anchors.right: parent.right
                                anchors.rightMargin: 3
                                anchors.top: parent.top
                                anchors.topMargin: 5
                                color: "green"
                                text: alive
                                visible: text !== "" && alive !== 0
                            }
                        }
                    }
                }

                Button {
                    id: addFlyButton
                    Material.background: "#ffb904"
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignHCenter

                    text: qsTr("Добавить муху!")
                    font.pixelSize: 16

                    enabled: fieldModel.canAddFly

                    onClicked: {
                        fieldModel.addFly()
                    }

                    contentItem: Text {
                        text: addFlyButton.text
                        font: addFlyButton.font
                        opacity: enabled ? 1.0 : 0.3
                        color: "#340000"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        elide: Text.ElideRight
                    }
                }
            }
        }
    }

    FieldModel {
        id: fieldModel
        onRowsInserted: {
            console.log("Inserted", rowCount())
        }
    }
}
