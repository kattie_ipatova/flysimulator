#ifndef FLYCONTROLLER_H
#define FLYCONTROLLER_H

#include <QObject>
#include <QMutex>
#include <QVector>
#include <QQueue>
#include <QMap>
#include <QSharedPointer>

#include "flyobject.h"
#include "sharedobjects.h"
#include "directiongenerator.h"

#include "structures.h"

typedef QWeakPointer<QMap<qint32, FliesInCell> > WeakFlyField;

/**
 * @brief Контролирует мух: проверяет запросы на добавление и движение,
 * ведёт подсчёт мух
 */
class FlyController : public QObject
{
    Q_OBJECT
public:
    FlyController(WeakFlyField _flyField, QObject* parent = 0);
    ~FlyController();
    void resize(); //Изменение размера поля

    void addFly(); // Запрос "добавить муху"

    int totalFlyCount() const; // Общее количество мух на поле
    bool canAddFly() const; // Возможно ли добавить ещё одну муху на поле

public slots:
    void stop(); //Останавливает выплнение

private:
    int findCell(); // Поиск свободной ячейки для добавления
    void handleMoving(FlyObject_ptr _fly,
                      int _to, int _from); //Обработка запроса на перемещение

private:
    QVector<Mutex_ptr> m_cellLockers; // Мьютексы на ячейку
    QMap<FlyObject_ptr/*fly*/, qint32/*cell*/> m_flies; // Мапа с мухами

    WeakFlyField m_flyField; // Weak-pointer на поле с мухами
    std::atomic<int> m_flyCount; // Общее количество мух
    std::atomic<int> m_fliesFinished; // Количество мух, закончивших свой жизненный путь

signals:
    void flyAdded(int _index);
    void flyMoved(int _from, int _to);
    void flyDied(int _index);

    void s_needStop(); //private
};

#endif // FLYCONTROLLER_H
