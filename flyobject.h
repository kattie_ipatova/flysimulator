#ifndef FLYOBJECT_H
#define FLYOBJECT_H

#include <QObject>
#include <QTimer>

#include <QThread>

#include "directiongenerator.h"
#include "sharedobjects.h"

/**
 * @brief Хранит состояния мухи
 */

class FlyObject : public QObject
{
    Q_OBJECT
public:
    FlyObject();
    ~FlyObject();

    void setIsDead(bool _isDead);
    bool isDead() const;
    qint32 age() const;
    bool isRunning() const;

public slots:
    void makeLifePath(); //Слот, представляющий собой жизненный процесс
    void stop(); // Останавливает выполнение

private slots:
    void makeAction(); //Создание "движения" мухи

private:
    bool m_isDead;
    qint32 m_age; // Количество "раундов", которые прожила муха
    bool m_needStop;
    bool m_isRunning;

signals:
    void s_isDeadChanged();
    void wannaMove(Direction::Direction direction);
    void s_finished();
};

#endif // FLYOBJECT_H
