#include "flyobject.h"
#include <QDebug>


FlyObject::FlyObject()
    : m_isDead(false), m_age(0), m_needStop(false), m_isRunning(false)
{
//    qDebug() << "Create fly request, stupidity time" << GeneralParameters::instance().stupidityMsec();
//    m_actionTimer.setSingleShot(false);
//    connect(&m_actionTimer, &QTimer::timeout,
//            this, &FlyObject::makeAction, Qt::QueuedConnection);
//    m_actionTimer.start(GeneralParameters::instance().stupidityMsec());
}

FlyObject::~FlyObject()
{
    m_needStop = true;
}

void FlyObject::setIsDead(bool _isDead)
{
    if (m_isDead == _isDead)
        return;
    m_isDead = _isDead;
    emit s_isDeadChanged();
}

bool FlyObject::isDead() const
{
    return m_isDead;
}

qint32 FlyObject::age() const
{
    return m_age;
}

bool FlyObject::isRunning() const
{
    return m_isRunning;
}

void FlyObject::stop()
{
    m_needStop = true;
}

void FlyObject::makeLifePath()
{
    qDebug() << QThread::currentThreadId() << "starts life path";
    m_isRunning = true;
    while (m_age < GeneralParameters::instance().matrixSize() && !m_needStop)
    {
        qDebug() << QThread::currentThreadId() << "Sleep";
        QThread::currentThread()->msleep(GeneralParameters::instance().stupidityMsec());

        qDebug() << QThread::currentThreadId() << "Awaking, prepare to make action";
        makeAction();

        qDebug() << QThread::currentThreadId() << "Maked";
    }
    m_isRunning = false;
    emit s_finished();
    qDebug() << QThread::currentThreadId() << "ends life path";
}

void FlyObject::makeAction()
{
    qDebug() << QThread::currentThreadId() << "request makeAction" << QThread::currentThreadId();
    ++m_age;
    if (m_age < GeneralParameters::instance().matrixSize())
    {
        qDebug() << QThread::currentThreadId() << "choosing direction" << QThread::currentThreadId();
        auto _direction = DirectionGenerator::getDirection();
        qDebug() << QThread::currentThreadId() << "direction choosed, emit signal" << QThread::currentThreadId();
        emit wannaMove(_direction);
        qDebug() << QThread::currentThreadId() << "signal emited" << QThread::currentThreadId();
    }
    else
    {
        qDebug() << QThread::currentThreadId() << "Fly is dead now";
        setIsDead(true);
    }
}
