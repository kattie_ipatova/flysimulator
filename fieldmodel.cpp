#include "fieldmodel.h"

#include <random>

#include <QDebug>
#include <QTime>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

FieldModel::FieldModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_cellMap.reset(new QMap<qint32, FliesInCell>());

    m_flyController = new FlyController(m_cellMap.toWeakRef());
    m_flyControllerThread = new QThread(this);
    m_flyController->moveToThread(m_flyControllerThread);
    connect(m_flyControllerThread, &QThread::finished, m_flyController, &QObject::deleteLater);
    connect(m_flyController, &FlyController::flyAdded, this, &FieldModel::handleAddFly);
    connect(m_flyController, &FlyController::flyMoved, this, &FieldModel::handleFlyMoved);
    connect(m_flyController, &FlyController::flyDied, this, &FieldModel::handleFlyDied);
    m_flyControllerThread->start();
}

FieldModel::~FieldModel()
{
    m_flyControllerThread->quit();
    m_flyControllerThread->wait();
}

int FieldModel::rowCount(const QModelIndex &_parent) const
{
    Q_UNUSED(_parent)
    return columns() * columns() ;
}

QVariant FieldModel::data(const QModelIndex &_index, int _role) const
{
    auto const& _actualIndex = _index.row();
    if (isIndexCorrect(_index))
    {
        switch (_role)
        {
        case ItemRoles::AliveCountRole:
        {
            if (m_cellMap->contains(_actualIndex))
                return m_cellMap->value(_actualIndex).aliveCount.load();
            return 0;
        }break;

        case ItemRoles::DeadCountRole:
        {

            if (m_cellMap->contains(_actualIndex))
                return m_cellMap->value(_actualIndex).deadCount.load();
            return 0;
        }break;
        }
    }
    return QVariant();
}

QHash<int, QByteArray> FieldModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ItemRoles::AliveCountRole] = "alive";
    roles[ItemRoles::DeadCountRole] = "dead";

    return roles;
}

int FieldModel::columns() const
{
    return GeneralParameters::instance().matrixSize();
}

bool FieldModel::canAddFly() const
{
    //qDebug() << "canAddFly: " <<
    return m_flyController->canAddFly();
}

void FieldModel::resize(qint32 _matrixSize, qint32 _flyCapacity, qint32 _stupidityMsec)
{
    beginRemoveRows(QModelIndex(), 0, rowCount());
    endRemoveRows();

    auto& _genParams = GeneralParameters::instance();
    _genParams.m_flyCapacity = _flyCapacity;
    _genParams.m_matrixSize = _matrixSize;
    _genParams.m_stupidityMsec = _stupidityMsec;
    m_flyController->resize();

    qDebug() << _flyCapacity << _matrixSize << _matrixSize;

    beginInsertRows(QModelIndex(), 0, _matrixSize*_matrixSize - 1);
    endInsertRows();

    emit columnsChanged();
    emit canAddFlyChanged();
}

void FieldModel::addFly()
{
    //TODO - check available rows

    m_flyController->addFly();
}

void FieldModel::handleAddFly(int _index)
{
    qDebug() << "Adding to cell" << _index;
    if (m_cellMap->contains(_index))
        ++m_cellMap->operator [](_index).aliveCount;
    else
        m_cellMap->insert(_index, FliesInCell(1, 0));
    emit dataChanged(index(_index, 0), index(_index, 0), {ItemRoles::AliveCountRole});
    emit canAddFlyChanged();
}

void FieldModel::handleFlyMoved(int _from, int _to)
{
    qDebug() << "Moving from" << _from << "to" << _to;
    if (m_cellMap->contains(_from))
        --m_cellMap->operator [](_from).aliveCount;
    else
        qWarning() << "Incorrect move: was no alive flies in cell" << _from;

    if (m_cellMap->contains(_to))
        ++m_cellMap->operator [](_to).aliveCount;
    else
        m_cellMap->insert(_to, FliesInCell(1,0));

    emit dataChanged(index(_from, 0), index(_from, 0), {ItemRoles::AliveCountRole});
    emit dataChanged(index(_to, 0), index(_to, 0), {ItemRoles::AliveCountRole});

}

void FieldModel::handleFlyDied(int _index)
{
    qDebug() << "Handle fly died";
    if (m_cellMap->contains(_index))
    {
        if (m_cellMap->value(_index).aliveCount > 0)
        {
            m_cellMap->operator [](_index).aliveCount--;
            m_cellMap->operator [](_index).deadCount++;
            emit dataChanged(index(_index, 0), index(_index, 0), {ItemRoles::AliveCountRole, ItemRoles::DeadCountRole});
        }
        else
            qWarning() << "No flies to die at" << _index;
    }
    else
        qWarning() << "Fly died, but no such index in model" << _index;
}

bool FieldModel::isIndexCorrect(const QModelIndex &_index) const
{
    return (_index.row() >= 0 && _index.row() < rowCount());
}
